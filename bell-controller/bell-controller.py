#!/usr/bin/env python

from flask import Flask, render_template, request
from flask_httpauth import HTTPBasicAuth
import RPi.GPIO as GPIO
import subprocess
import time

app = Flask(__name__)
auth = HTTPBasicAuth()
GPIO.setmode(GPIO.BCM)

pins = {
   16 : {'name' : 'GPIO 16', 'state' : GPIO.LOW},
   }

users = {
   0 : {'username' : 'bells', 'password' : 'mercyreigns'}
}

# Set each pin as an output and make it low:
for pin in pins:
   GPIO.setup(pin, GPIO.OUT)
   GPIO.output(pin, GPIO.LOW)

@auth.verify_password
def verify_password(username, password):
    if username == users[0]['username']:
        return password == users[0]['password']
    return False

@app.route("/")
@auth.login_required
def main():
   # For each pin, read the pin state and store it in the pins dictionary:
   crontab = subprocess.Popen(["crontab", "-l"],
            stdout=subprocess.PIPE, 
            stderr=subprocess.STDOUT
            )
   stdout,stderr = crontab.communicate()

   schedule = stdout.split('\n')

   for pin in pins:
      pins[pin]['state'] = GPIO.input(pin)

   # Put the pin dictionary into the template data dictionary:
   templateData = {
      'pins' : pins,
      'crontab' : schedule[0]
      }
   # Pass the template data into the template main.html and return it to the user
   return render_template('main.html', **templateData)

# The function below is executed when someone requests a URL with the pin number and action in it:
@app.route("/ringbell/<action>")
@auth.login_required
def ringbell(action):
   if action == "ring":
      # Set the pin high:
      GPIO.output(16, GPIO.HIGH)
      # Ring for 5 seconds:
      time.sleep(5)
      # Set the pin low:
      GPIO.output(16, GPIO.LOW)
      # Save the status message to be passed into the template:
      message = "Rang bell from button."

   # Along with the pin dictionary, put the message into the template data dictionary:
   templateData = {
      'pins' : pins,
      #'crontab' : stdout,
      'message' : message
   }

   return render_template('main.html', **templateData)

# The function below is executed when someone requests a URL with the pin number and action in it:
@app.route("/changeSchedule/<action>")
@auth.login_required
def changeSchedule(action):
   if action == "2hrdelay":
      crontab = subprocess.Popen(["crontab", "/usr/local/bin/bell-controller/schedules/2hrdelaycrontab"],
               stdout=subprocess.PIPE, 
               stderr=subprocess.STDOUT
               )
      stdout,stderr = crontab.communicate()
      message = "Updated schedule to " + action
   if action == "normal":
      crontab = subprocess.Popen(["crontab", "/usr/local/bin/bell-controller/schedules/normalcrontab"],
               stdout=subprocess.PIPE, 
               stderr=subprocess.STDOUT
               )
      stdout,stderr = crontab.communicate()
      message = "Updated schedule to " + action
   if action == "disable":
      crontab = subprocess.Popen(["crontab", "/usr/local/bin/bell-controller/schedules/disablebellscrontab"],
               stdout=subprocess.PIPE, 
               stderr=subprocess.STDOUT
               )
      stdout,stderr = crontab.communicate()
      message = "Updated schedule to " + action

   templateData = {
      'pins' : pins,
      'crontab' : stdout,
      'message' : message
   }

   return render_template('main.html', **templateData)

if __name__ == "__main__":
   app.run(host='0.0.0.0', port=80, debug=True)
