mv bell-controller folder in /usr/local/bin/
mv ringbell /usr/local/bin/
mv bell-controller-init.d /etc/init.d/bell-controller
chmod 755 /usr/local/bin/bell-controller/bell-controller.py
chmod 755 /usr/local/bin/ringbell
chmod 755 /etc/init.d/bell-controller
update-rc.d bell-controller defaults
/etc/init.d/bell-controller start